# CMake generated Testfile for 
# Source directory: /home/jackson/catkin_ws/src
# Build directory: /home/jackson/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("general-message-pkgs/path_navigation_msgs")
subdirs("general-message-pkgs/object_msgs")
subdirs("gazebo_ros_link_attacher")
subdirs("gazebo-pkgs/gazebo_test_tools")
subdirs("gazebo-pkgs/gazebo_version_helpers")
subdirs("gazebo-pkgs/gazebo_grasp_plugin")
subdirs("gazebo-pkgs/gazebo_grasp_plugin_ros")
subdirs("gazebo-pkgs/gazebo_world_plugin_loader")
subdirs("general-message-pkgs/object_msgs_tools")
subdirs("gazebo-pkgs/gazebo_state_plugins")
subdirs("roboticsgroup_gazebo_plugins")
subdirs("ur5e_robotiq2f140_demo")
